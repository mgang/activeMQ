package com.mg.jfinal.ext.demo;

import org.quartz.Job;

import com.jfinal.ext.plugin.jms.JmsPlugin;
import com.jfinal.ext.plugin.quartz.QuartzPlugin;
import com.mg.jfinal.ext.PropertyConfig;

public class TestJMS {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		JmsPlugin jp = new JmsPlugin("jms.properties");
		jp.start();
		PropertyConfig pc = PropertyConfig.me();
		pc.loadPropertyFile("job.properties");
		QuartzPlugin qp = new QuartzPlugin();
		if (pc.getPropertyToBoolean("a.enable")) {
			qp.add(pc.getProperty("a.cron"), (Job) Class.forName(pc.getProperty("a.job")).newInstance());
	    }
		qp.start();
	}
	
	
}
