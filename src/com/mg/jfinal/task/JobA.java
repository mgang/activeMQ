package com.mg.jfinal.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jfinal.ext.plugin.jms.JmsKit;
import com.mg.jfinal.ext.demo.Person;

public class JobA implements Job{
	static int age = 22;
	public void run() {
		/*Person p = new Person("小刚",age);
		JmsKit.sendQueue("firstMQ", p, "a");
		age ++;*/
		System.out.println(age);
	}
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		Person p = new Person("小刚",age);
		JmsKit.sendQueue("firstMQ", p, "a");
		age ++;
	}

}
